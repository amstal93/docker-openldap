#!/bin/bash
set -e && source /opt/bitnami/scripts/libopenldap.sh

# - Enable access to password fiels by administrators and self
# - Enable full write access for administrators
# - Enable full read access for cn=system
# - Disable anonymous access

# TODO ACL for user profile update by self
# TODO ACL for managers to update the subtrees users and groups
# TODO ACL for users read ?

info "Setup ACLs"
ldapmodify -Q -Y EXTERNAL -H "ldapi:///" > /dev/null <<EOF
dn: olcDatabase={2}mdb,cn=config
changetype: modify
replace: olcAccess
olcAccess: {0}to attrs=userPassword,shadowLastChange 
  by dn="$LDAP_ADMIN_DN" write
  by dn="cn=system,$LDAP_ROOT" read
  by group.exact="cn=ldapadmins,ou=groups,$LDAP_ROOT" write
  by group.exact="cn=ldapmanagers,ou=groups,$LDAP_ROOT" write
  by self write
  by anonymous auth
  by * none
olcAccess: {1}to attrs=sn,givenName,displayName,mail,sshPublicKey,loginShell
  by dn="$LDAP_ADMIN_DN" write
  by dn="cn=system,$LDAP_ROOT" read
  by group.exact="cn=ldapadmins,ou=groups,$LDAP_ROOT" write
  by self write
  by * none
olcAccess: {2}to *
  by dn="$LDAP_ADMIN_DN" write
  by dn="cn=system,$LDAP_ROOT" read
  by group.exact="cn=ldapadmins,ou=groups,$LDAP_ROOT" write
  by users read
  by * none
EOF
