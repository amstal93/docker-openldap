#!/bin/bash
set -e && source /opt/bitnami/scripts/libopenldap.sh

info "Creating increment counter for UIDs"
ldapadd -D $LDAP_ADMIN_DN -w $LDAP_ADMIN_PASSWORD -H "ldapi:///" > /dev/null <<EOF
dn: cn=uidNext,$LDAP_ROOT
objectClass: uidNext
cn: uidNext
uidNumber: 2000
EOF

info "Creating increment counter for GIDs"
ldapadd -D $LDAP_ADMIN_DN -w $LDAP_ADMIN_PASSWORD -H "ldapi:///" > /dev/null <<EOF
dn: cn=gidNext,$LDAP_ROOT
objectClass: uidNext
cn: gidNext
uidNumber: 3000
EOF
