#!/bin/bash
set -e && source /opt/bitnami/scripts/libopenldap.sh

# MemberOf overlay for managing groups (auto attr update for entries)
# https://tylersguides.com/guides/openldap-memberof-overlay/
info "Enable and configure memberOf module..."

ldapadd -Q -Y EXTERNAL -H "ldapi:///" > /dev/null <<EOF
dn: cn=module{0},cn=config
objectClass: olcModuleList
cn: module{0}
olcModuleLoad: memberof
EOF

ldapadd -Q -Y EXTERNAL -H "ldapi:///" > /dev/null <<EOF
dn: olcOverlay=memberof,olcDatabase={2}mdb,cn=config
objectClass: olcOverlayConfig
objectClass: olcMemberOf
olcOverlay: memberof
olcMemberOfRefint: TRUE
EOF
