#!/bin/bash
set -e && source /opt/bitnami/scripts/libopenldap.sh

# https://tobru.ch/openldap-password-policy-overlay/#:~:text=OpenLDAP%20has%20a%20dynamically%20loadable,a%20password%20and%20many%20more.
# https://www.zytrax.com/books/ldap/ch6/ppolicy.html#pwdpolicyattributes
info "Configure default password policy"

ldapadd -D $LDAP_ADMIN_DN -w $LDAP_ADMIN_PASSWORD -H "ldapi:///" > /dev/null <<EOF
dn: cn=ppolicy,$LDAP_ROOT
objectClass: device
objectClass: pwdPolicyChecker
objectClass: pwdPolicy
cn: ppolicy
pwdAllowUserChange: TRUE
pwdAttribute: userPassword
pwdCheckQuality: 2
pwdExpireWarning: 600
pwdFailureCountInterval: 30
pwdGraceAuthNLimit: 5
pwdInHistory: 5
pwdLockout: TRUE
pwdLockoutDuration: 30
pwdMaxAge: 0
pwdMaxFailure: 5
pwdMinAge: 0
pwdMinLength: 8
pwdMustChange: FALSE
pwdSafeModify: FALSE
EOF
