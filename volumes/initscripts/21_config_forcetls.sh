#!/bin/bash
set -e && source /opt/bitnami/scripts/libopenldap.sh

info "Force TLS connexion"
ldapmodify -Q -Y EXTERNAL -H "ldapi:///" > /dev/null <<EOF
dn: cn=config
changetype: modify
add: olcSecurity
olcSecurity: ssf=36
EOF
