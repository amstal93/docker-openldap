#!/bin/bash
set -e && source /opt/bitnami/scripts/libopenldap.sh

# Extra schema for uid autoincrement
# https://rexconsulting.net/2011/11/28/ldap-protocol-uidnumber-html/
# https://www.perlmonks.org/?node_id=457108
info "Creating dedicated schema for uidNumber autoincrement"

ldapadd -Q -Y EXTERNAL -H "ldapi:///" > /dev/null <<EOF
dn: cn=uidNext,cn=schema,cn=config
objectClass: olcSchemaConfig
cn: uidNext
olcObjectClasses: {0}( 1.3.6.1.4.1.19173.2.2.2.8
  NAME 'uidNext'
  DESC 'Where we get the next uidNumber from'
  MUST ( cn $ uidNumber ) )
EOF
