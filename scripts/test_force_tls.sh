#!/bin/bash
source .env

echo "This command should fail after authentication with 'Confidentiality required (13)'..."
docker-compose exec -e LDAPTLS_REQCERT=never openldap ldapsearch -x -H ldap://openldap:1389 -b $LDAP_ROOT -D uid=admininistrator,$LDAP_ROOT -w blahblahblah

if [ "$?" != "13" ] ; then
  echo "!!! Error, LDAP server does not enforce TLS"
  exit 1
fi
